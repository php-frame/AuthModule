<?php
namespace Frame\Module\Auth\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'username', 
        'email', 
        'password', 
        'active', 
        'active_hash', 
        'remember_identifier', 
        'remember_token', 
        'recover_hash'
    ];

    public function updateRememberCredentials($identifier, $token)
    {
        $this->update([
            'remember_identifier' => $identifier,
            'remember_token' => $token,
        ]);
    }

    public function removeRememberCredentials()
    {
        $this->updateRememberCredentials(null, null);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id');
    }

    public function hasRole($role)
    {
        foreach($this->roles as $r){
            if($r->label == $role){
                return true;
            }
        }

        return false;
    }

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }
}