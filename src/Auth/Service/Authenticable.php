<?php 
namespace Frame\Module\Auth\Service;

use Exception;

use Frame\Traits\ContainerAwareTrait;

use Frame\Module\Auth\Model\User;
use Frame\Util\Cookie;
use Frame\Util\Session;

use Carbon\Carbon;

class Authenticable{
	use ContainerAwareTrait;

	public function login($login, $password, $remember=null){
		// User check
        $user = User::where('username', $login)->orWhere('email', $login)->first();

        if(!$user || !$this->hash->verifyPassword($password, $user->password)) {
            $this->flash->addMessageNow("error", $this->translator->lang('@Auth.login.invalid'));
            throw new Exception;
        } 
        else if(!(bool)$user->active) {
            Session::set('temp_user_id', $user->id);
            $this->flash->addMessage("warning", $this->translator->lang('@Auth.account.activation.must', [ 'link' => $this->router->pathFor('auth.activate.resend') ]));
            throw new Exception;
        } 
        else if($this->hash->verifyPassword($password, $user->password)) {
            Session::set($this->dotGet('config.app.auth_id'), $user->id);
            
            if(Session::exists('temp_user_id')) {
                Session::destroy('temp_user_id');
            }

            if($remember === "on") {
                $rememberIdentifier = $this->hash->generate(128);
                $rememberToken = $this->hash->generate(128);

                $user->updateRememberCredentials($rememberIdentifier, $this->hash->hash($rememberToken));

                Cookie::set(
                    $this->dotGet('config.app.remember_id'), 
                    "{$rememberIdentifier}.{$rememberToken}"
                );
            }

            $this->flash->addMessage("success", $this->translator->lang('@Auth.login.success', [ 'user' => $user->username ]));

            $this->SsoService->persist($user->id);
        }

        return $user;
	}

	public function register($username, $email, $password){
		$activeHash = $this->hash->generate(128);

        if(User::where('username', $username)->first() != null){
            $this->flash->addMessageNow('error', $this->translator->lang('form.error'));

            $this->ViewData->merge(['errors' => ['username' => $this->translator->lang('@Auth.registration.username_used')]]);

            throw new Exception;
        }

        if(User::where('email', $email)->first() != null){
            $this->flash->addMessageNow('error', $this->translator->lang('form.error'));

            $this->ViewData->merge(['errors' => ['email' => $this->translator->lang('@Auth.registration.email_used')]]);

            throw new Exception;
        }

        $user = User::create([
            'username' => $username,
            'email' => $email,
            'password' => $this->hash->password($password),
            'active_hash' => $activeHash,
            'active' => true
        ]);

        $this->flash->addMessage('success', $this->translator->lang('@Auth.registration.success'));

        return $user;
	}

    public function logout(){
        if(Cookie::exists(env('APP_REMEMBER_ID', 'APP_REMEMBER_TOKEN'))) {
            $this->auth->removeRememberCredentials();
            Cookie::destroy(env('APP_REMEMBER_ID', 'APP_REMEMBER_TOKEN'));
        }

        Session::destroy(env('APP_AUTH_ID', 'user_id'));

        $this->flash->addMessage("success", $this->translator->lang('@Auth.logout.success'));
    }

    public function forgotPassword($email){
        $user = User::where('email', $email)->first();

        if($user) {
            $identifier = $this->hash->generate();

            $user->update([
                'recover_hash' => $identifier
            ]);

            $this->mail->send('@Auth/mail/password/forgot.twig', ['identifier' => $identifier, 'date' => \Carbon\Carbon::now()->toFormattedDateString(), 'user' => $user], function($message) use ($user) {
                $message->to($user->email);
                $message->subject($this->translator->lang('@Auth.mail.password.forgot.subject'));
            });
        }

        $this->flash->addMessage("info", $this->translator->lang('@Auth.password.forgot'));
    }

    public function resetPassword($newPassword, $identifier){
        $user = $this->auth->where('recover_hash', $identifier)->first();

        if(!$user) {
            $this->flash->addMessage('error', $this->translator->lang('@Auth.password.reset.invalid'));
            throw new Exception;
        }

        $user->update([
            'recover_hash' => null,
            'password' => $this->hash->password($newPassword),
        ]);

        $this->flash->addMessage('success', $this->translator->lang('@Auth.password.reset.success'));
        return $user;
    }

    public function updatePassword($currentPassword, $newPassword){

        if(!$this->hash->verifyPassword($currentPassword, $this->auth->password)){
            $this->flash->addMessageNow('error', $this->translator->lang('form.error'));
            $this->ViewData->merge(['errors' => ['current_password' => $this->translator->lang('@Auth.messages.wrong_pass')]]);
            throw new Exception;
        }

        $this->auth->update([
            'password' => $this->hash->password($newPassword),
        ]);

        $this->flash->addMessage('success', $this->translator->generic('updated', [ 'label' => '@Auth.parts.password' ]));
    }
}