<?php 
namespace Frame\Module\Auth\Service;

use Frame\Util\Cookie;
use Frame\Module\Auth\Model\User;

/**
 * SSO service class
 * Providing sso data generation and shared cookie management
 */
class SsoService{
	/**
	 * The shared cookie key name
	 */
	const COOKIE_NAME = 'SSO_TOKEN';

	/**
	 * @var int $tokenLength The length of the generated token
	 */
	protected $tokenLength = 16;

	/**
	 * @var string $data The encrypted data sended to the target service
	 */
	private $data = null;

	/**
	 * Service constructor
	 * 
	 * @param \Frame\Module\Auth\Model\User $user The logged in user
	 */
	public function __construct($user){
		$this->user = $user;
	}

	/**
	 * Function that encrypt the given data
	 * 
	 * @param  mixed $value The value to encrypt
	 * @return void
	 */
	protected function build(){
		return bin2hex(openssl_random_pseudo_bytes($this->tokenLength/2));
	}

	/**
	 * Check if passed token equals to logged user token
	 * 
	 * @param string $token The passed token
	 * @return bool 
	 */
	public function check($token){
		return $token == $this->get() && $this->get() != null;
	}

	/**
	 * Cookie creation with encrypted data
	 * 
	 * @param  mixed $value The value to put into the cookie
	 * @return void
	 */
	public function persist($id){
		if(!Cookie::exists(self::COOKIE_NAME)){
			$this->data = $this->build();

			Cookie::set(self::COOKIE_NAME, $this->data, [
				'domain' => env('SSO_DOMAIN', null)
			]);
			User::find($id)->update(['sso_token' => $this->data]);
		}
	}

	/**
	 * Cookie deletion if the user is not logged in
	 * 
	 * @return void
	 */
	public function clean(){
		if(Cookie::exists(self::COOKIE_NAME)){
			Cookie::destroy(self::COOKIE_NAME);
			$this->user->update(['sso_token' => null]);
		}
	}

	/**
	 * Retrieve the encrypted data if it exist
	 * 
	 * @return mixed|null The encrypted data
	 */
	public function get(){
		if(Cookie::exists(self::COOKIE_NAME)){
			return Cookie::get(self::COOKIE_NAME);
		}
		return null;
	}
}