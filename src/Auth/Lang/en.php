<?php 

return [
    'parts' => [
        'password' => 'Password',
        'profile' => 'Profile',
        'login' => 'Login',
        'register' => 'Register',
        'settings' => 'Settings',
        'logout' => 'Logout',
        'form' => [
            'current_password' => 'Current password',
            'new_password' => 'New password',
            'confirm_new_password' => 'Confirm new password',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'remember' => 'Remember me',
            'password_forgot' => 'Forgot password?'
        ]
    ],
    'login' => [
        'success' => 'Succesfully logged in. Welcome {{ user }} !',
        'already' => 'You are already logged in.',
        'must' => 'You must be logged in to access that page.',
        'invalid' => 'You have supplied invalid credentials.',
        'error' => 'Please enter properly your credentials to continue.'
    ],
    'password' => [
        'forgot' => 'If your email is valid, you\'ll receive an email with instructions on how to reset your password.',
        'reset' => [
            'success' => 'You have successfully reset your password. You can now login with your new password.',
            'fail' => 'Your password could not be reset at this time.',
            'invalid' => 'Your password reset token was invalid. Please submit another password reset request.'
        ]
    ],
    'registration' => [
        'success' => 'Your account has been created!',
        'error' => 'Please fix any errors with your registration and try again.',
        'username_used' => 'Username already in use',
        'email_used' => 'Email already in use'
    ],
    'account' => [
        'activation' => [
            'success' => 'Your account has been activated ! You can now login.',
            'already' => 'Your account has already been activated.',
            'invalid' => 'The active hash you are trying to use has already expired or never existed.',
            'resend' => 'Another activation e-mail has been sent. Please check your e-mail for instructions.',
            'must' => 'The account you are trying to access has not been activated. <a href="{{ url }}">Resend activation link</a>',
            'check' => 'Your account has been created but you will need to activate it. Please check your e-mail for instructions.'
        ],

        'profile' => [
            'update' => 'Your profile has been updated!',
            'fail' => 'Your profile couldn\'t be updated at this time.',
        ]
    ],
    'mail' => [
        'activation' => [
            'subject' => env('APP_NAME', 'My App') . ' Account Activation',
        ],

        'password' => [
            'forgot' => [
                'subject' => env('APP_NAME', 'My App') . ' Password Reset Request',
            ]
        ],
    ],
    'messages' => [
        'admin_login' => 'This page is dedicated to the website owner.',
        'register' => 'Don\'t have an account? <a href="{{ url }}">Register</a> for one.',
        'wrong_pass' => 'Wrong password'
    ]
];