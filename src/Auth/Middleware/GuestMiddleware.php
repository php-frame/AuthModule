<?php
namespace Frame\Module\Auth\Middleware;

use Frame\Middleware\Middleware;

class GuestMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if($this->container->auth->check()) {
        	$this->flash('warning', $this->translator->lang('@Auth.login.already'));
            return $this->back($request, $response);
        }
        
        $response = $next($request, $response);
        return $response;
    }
}
