<?php
namespace Frame\Module\Auth\Middleware;

use Frame\Middleware\Middleware;
use Frame\Module\Auth\Service\SsoService;

class SsoMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if($this->auth->check()){
            $this->SsoService->persist($this->auth->userId());
        }
        else{
            $this->SsoService->clean();
        }
        
        if($request->getParam('sso_redirect') != null){
            $this->ViewData->merge([ 'sso_redirect' => $request->getParam('sso_redirect') ]);
        }
        if($request->getParam('sso_redirect') != null && $this->auth->check()){
            return $this->rawRedirect($response, $request->getParam('sso_redirect') . '?' . http_build_query(['sso_data' => SsoService::getSsoData()]));
        }
        
        $response = $next($request, $response);

        if($request->getParam('sso_redirect') != null && $this->auth->check()){
        	return $this->rawRedirect($response, $request->getParam('sso_redirect') . '?' . http_build_query(['sso_data' => SsoService::getSsoData()]));
        }

        return $response;
    }
}


