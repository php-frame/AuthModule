<?php
namespace Frame\Module\Auth\Middleware;

use Frame\Middleware\Middleware;

class AuthMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if(!$this->container->auth->check()) {
            $this->flash('warning', $this->translator->lang('@Auth.login.must'));
            return $this->redirect($response, '@Auth.login');
        }
        
        $response = $next($request, $response);
        return $response;
    }
}
