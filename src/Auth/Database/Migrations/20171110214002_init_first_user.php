<?php


use Phinx\Migration\AbstractMigration;

class InitFirstUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->insert('users', [
            'id' => 1,
            'username' => env('ADMIN_NAME', 'admin'),
            'email' => env('ADMIN_MAIL', 'admin@admin.com'),
            'password' => (new Frame\Util\Hash)->password(env('ADMIN_PASSWORD', 'admin')),
            'active' => true,
        ]);
    }
}
