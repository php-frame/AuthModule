<?php


use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('users')
            ->addColumn('username', 'text', [ 'limit' => 50 ])
            ->addColumn('email', 'text', [ 'limit' => 50 ])
            ->addColumn('password', 'text', [ 'limit' => 255 ])
            ->addColumn('active', 'boolean')
            ->addColumn('active_hash', 'text', [ 'limit' => 255, 'null' => true ])
            ->addColumn('remember_token', 'text', [ 'limit' => 255, 'null' => true ])
            ->addColumn('remember_identifier', 'text', [ 'limit' => 255, 'null' => true ])
            ->addColumn('recover_hash', 'text', [ 'limit' => 255, 'null' => true ])
            ->addColumn('created_at', 'timestamp', [ 'default' => 'CURRENT_TIMESTAMP' ])
            ->addColumn('updated_at', 'timestamp', [ 'default' => 'CURRENT_TIMESTAMP' ])
            ->addIndex(['email'], [ 'unique' => true, 'limit' => 50 ])
            ->addIndex(['username'], [ 'unique' => true, 'limit' => 50 ])
            ->create();
    }
}
