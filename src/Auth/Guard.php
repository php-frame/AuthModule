<?php
namespace Frame\Module\Auth;

use Frame\Util\Session;
use Frame\Module\Auth\Model\User;

class Guard extends User
{
    public function check()
    {
        return Session::exists(env('APP_AUTH_ID', 'user_id'));
    }

    public function user()
    {
        return self::find($this->userId());
    }

    public function userId(){
         return Session::get(env('APP_AUTH_ID', 'user_id'));
    }
}