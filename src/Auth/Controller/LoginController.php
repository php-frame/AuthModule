<?php
namespace Frame\Module\Auth\Controller;

use Exception;

use Frame\Controller\Controller;
use Frame\Validation\Rule;
use Frame\Module\Auth\Service\Authenticable;

class LoginController extends Controller
{
    public function get()
    {
        return $this->render('@Auth/login');
    }

    public function post()
    {
        if(!$this->validate([
            (new Rule('username'))->notEmpty()->stringType(),
            (new Rule('password'))->notEmpty()->stringType()
        ])){
            return $this->render('@Auth/login');
        }

        $username = $this->param('username');
        $password = $this->param('password');
        $remember = $this->param('remember');

        try{
            (new Authenticable($this->container))->login($username, $password, $remember);
        }
        catch(Exception $e){
            return $this->get();
        }
        
        return $this->back();
    }
}
