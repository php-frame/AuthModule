<?php
namespace Frame\Module\Auth\Controller;

use Exception;

use Frame\Controller\Controller;
use Frame\Validation\Rule;
use Frame\Module\Auth\Service\Authenticable;

class RegisterController extends Controller
{
    public function get()
    {
        return $this->render('@Auth/register');
    }

    public function post()
    {
        if(!$this->validate([
            (new Rule('username'))->notEmpty()->stringType(),
            (new Rule('email'))->notEmpty()->email(),
            (new Rule('password'))->notEmpty()->stringType(),
            (new Rule('confirm_password'))->notEmpty()->equals($this->param('password'))
        ])){
            return $this->render('@Auth/register');
        }

        $username = $this->param('username');
        $email = $this->param('email');
        $password = $this->param('password');
        
        try{
            (new Authenticable($this->container))->register($username, $email, $password);
        }
        catch(Exception $e){
            return $this->get();
        }

        if($this->param('sso_redirect') != null){
            return $this->redirect('@Auth.login', [], ['sso_redirect' => $this->param('sso_redirect')]);
        }

        return $this->redirect('@Auth.login');
    }
}
