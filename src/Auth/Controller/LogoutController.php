<?php
namespace Frame\Module\Auth\Controller;

use Frame\Controller\Controller;
use Frame\Module\Auth\Service\Authenticable;

class LogoutController extends Controller
{
    public function get()
    {
        (new Authenticable($this->container))->logout();

        return $this->response->withRedirect(env('APP_URI'));
    }
}
