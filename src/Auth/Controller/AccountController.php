<?php
namespace Frame\Module\Auth\Controller;

use Frame\Controller\Controller;

use Frame\Module\Auth\Model\User;
use Frame\Module\Auth\Service\Authenticable;
use Frame\Validation\Rule;

use Respect\Validation\Validator;

class AccountController extends Controller
{
    public function get()
    {
        return $this->render('@Auth/account');
    }

    public function getSettings(){
        return $this->render('@Auth/account/settings');
    }

    public function postProfile()
    {
        $email = $this->param('email');

        if($this->param('email') != null && $this->param('email') != $this->auth->email){
            if(!$this->validate([
                (new Rule('email'))->email()->length(1, 50)->unique(User::class, 'email')
            ])){
                return $this->getSettings();
            }

            $this->auth->update([
                'email' => $email,
            ]);

            $this->flash('success', $this->translator->generic('updated', [ 'label' => '@Auth.parts.form.email' ]));
        }

        if($this->param('username') != null && $this->param('username') != $this->auth->username){
            if(!$this->validate([
                (new Rule('username'))->stringType()->length(1, 50)->unique(User::class, 'username')
            ])){
                return $this->getSettings();
            }

            $this->auth->update([
                'username' => $username,
            ]);

            $this->flash('success', $this->translator->generic('updated', [ 'label' => '@Auth.parts.form.username' ]));
        }

        
        return $this->redirect('@Auth.account.settings');
    }

    public function postPassword()
    {
        if(!$this->validate([
            (new Rule('current_password'))->notEmpty(),
            (new Rule('new_password'))->notEmpty()->not(Validator::equals($this->param('current_password'))),
            (new Rule('confirm_new_password'))->notEmpty()->equals($this->param('new_password'))->setTemplate('passwords dont match')
        ])){
            return $this->getSettings();
        }

        $currentPassword = $this->param('current_password');
        $newPassword = $this->param('new_password');

        try{
            (new Authenticable($this->container))->updatePassword($currentPassword, $newPassword);
        }
        catch(\Exception $e){
            return $this->getSettings();
        }

        return $this->redirect('@Auth.account.settings');
    }
}
