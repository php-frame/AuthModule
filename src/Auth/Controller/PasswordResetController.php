<?php
namespace Frame\Module\Auth\Controller;

use Frame\Controller\Controller;
use Frame\Validation\Rule;
use Frame\Module\Auth\Service\Authenticable;

class PasswordResetController extends Controller
{
    public function getForgot()
    {
        return $this->render('@Auth/password/forgot');
    }

    public function postForgot()
    {
        if(!$this->validate([
            (new Rule('email'))->notEmpty()->email()
        ])){
            return $this->render('@Auth/password/forgot');
        }

        $email = $this->param('email');

        (new Authenticable($this->container))->forgotPassword($email);

        return $this->redirect('@Auth.login');
    }

    public function getReset()
    {
        if(!$this->validate([
            (new Rule('identifier'))->notEmpty()          
        ])){
            return $this->render('@Auth/password/forgot');
        }

        return $this->render('@Auth/password/reset', [
            'identifier' => $this->param('identifier')
        ]);
    }

    public function postReset()
    {
        if(!$this->validate([
            (new Rule('new_password'))->notEmpty()->stringType(),
            (new Rule('confirm_new_password'))->notEmpty()->equals($this->param('new_password')),
            (new Rule('identifier'))->notEmpty()
        ])){
            return $this->render('@Auth/password/reset', [ 'identifier' => $this->param('identifier') ]);
        }

        $identifier = $this->param('identifier');
        $newPassword = $this->param('new_password');

        (new Authenticable($this->container))->resetPassword($newPassword, $identifier);

        return $this->redirect('@Auth.login');
    }
}
