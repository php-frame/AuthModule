<?php
namespace Frame\Module\Auth\Controller;

use Frame\Controller\Controller;

use Frame\Validation\Rule;

class SsoController extends Controller
{
    public function getData()
    {
    	if(!$this->validate([
    		(new Rule('sso_token'))->notEmpty()->stringType()
    	])){
    		return $this->response->withStatus(403);
    	}

        if(!$this->SsoService->check($this->param('sso_token'))){
        	return $this->response->withStatus(403);
        }

        return $this->response->withJson(['this' => 'is data']);
    }
}
