<?php
namespace Frame\Module;

// Core
use Frame\Util\Cookie;
use Frame\Util\Session;

// Controllers
use Frame\Module\Auth\Controller\LoginController;
use Frame\Module\Auth\Controller\LogoutController;
use Frame\Module\Auth\Controller\RegisterController;
use Frame\Module\Auth\Controller\AccountController;
use Frame\Module\Auth\Controller\ActivationController;
use Frame\Module\Auth\Controller\PasswordResetController;
use Frame\Module\Auth\Controller\SsoController;

// Middlewares
use Frame\Middleware\FeatureNotAvailable;
use Frame\Module\Auth\Middleware\GuestMiddleware;
use Frame\Module\Auth\Middleware\AuthMiddleware;
use Frame\Module\Auth\Middleware\SsoMiddleware;
use Frame\Module\Auth\Middleware\RememberMiddleware;

// Services
use Frame\Module\Auth\Service\SsoService;

class Auth extends \Frame\Module
{
	const MIGRATIONS = __DIR__ . '/Auth/Database/Migrations';
	const VIEWS = __DIR__ . '/Auth/Ressources/views';
	const LANGUAGES = __DIR__ . '/Auth/Lang';

    public function __invoke(){
    	$c = $this->app->getContainer();

		$auth = new \Frame\Module\Auth\Guard;
		if($auth->check()){
			$auth = $auth->user();
		}
		$c['auth'] = $auth;
		$c['SsoService'] = new SsoService($auth);

		$c->view->getEnvironment()->addGlobal('auth', $c->auth);

		$this->app->add(RememberMiddleware::class);
		$this->app->add(SsoMiddleware::class);

        $this->app->map(['GET', 'POST'], '/admin/auth/settings', LoginController::class)->setName($this->prefix . '.settings');

		// Routing
		$that = $this;
		$this->app->group('/auth', function() use ($that) {
		    $this->map(['GET'], '/activate', ActivationController::class)->setName($that->prefix . '.activate');
		    $this->map(['GET'], '/activate/resend', ActivationController::class . ':resend')->setName($that->prefix . '.activate.resend');

			$this->group('', function() use ($that) {
			    $this->map(['GET'], '/account', AccountController::class)->setName($that->prefix . '.account');
			    $this->map(['GET'], '/account/settings', AccountController::class . ':settings')->setName($that->prefix . '.account.settings');
			    $this->map(['POST'], '/account/profile', AccountController::class . ':profile')->setName($that->prefix . '.account.profile');
			    $this->map(['POST'], '/account/password', AccountController::class . ':password')->setName($that->prefix . '.account.password');
		    	$this->get('/logout', LogoutController::class)->setName($that->prefix . '.logout');
			})->add(AuthMiddleware::class);

			$this->group('', function() use ($that) {
			    $this->map(['GET', 'POST'], '/password/forgot', PasswordResetController::class . ':forgot')->setName($that->prefix . '.password.forgot');
			    $this->map(['GET', 'POST'], '/password/reset', PasswordResetController::class . ':reset')->setName($that->prefix . '.password.reset');
			})->add(GuestMiddleware::class);
		    
		    $this->group('', function() use ($that) {
			    $this->map(['GET', 'POST'], '/login', LoginController::class)->setName($that->prefix . '.login');
			    $this->map(['GET', 'POST'], '/register', RegisterController::class)->setName($that->prefix . '.register');
			})->add(GuestMiddleware::class);
		});

		$this->app->group('/sso', function() use ($that) {
			$this->map(['GET'], '/data', SsoController::class . ':data')->setName($that->prefix . '.sso.data');
		});

		// View nav update
        $c->ViewData->merge([
			[
				'file' => '@Auth/parts/nav/login-item.twig'
			],
        ], ['navbar', 'right']);
    }
}