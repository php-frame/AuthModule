<?php
use PHPUnit\Framework\TestCase;
use Slim\Http\Request;

class AppTest extends TestCase
{	
	protected $app;

	protected function setUp()
    {
        $_SESSION = [];
        $this->app = new \Frame\Core([ \Frame\Module\Auth::class ]);
    }

    public function testSetup()
    {
        $env = \Slim\Http\Environment::mock([
            'REQUEST_URI' => '/auth/login'
        ]);
        $this->app->getContainer()['request'] = Request::createFromEnvironment($env);
        $this->app->run(true);
        $this->assertNotEmpty($this->app);
    }

    public function testLoginSuccess(){
        $this->app->getContainer()->get('csrf')->generateToken();

        $env = \Slim\Http\Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/auth/login',
            'QUERY_STRING' => http_build_query([
                $this->app->getContainer()->get('csrf')->getTokenNameKey() => $this->app->getContainer()->get('csrf')->getTokenName(),
                $this->app->getContainer()->get('csrf')->getTokenValueKey() => $this->app->getContainer()->get('csrf')->getTokenValue(),
                'username' => env('ADMIN_MAIL'),
                'password' => env('ADMIN_PASSWORD')
            ])
        ]);

        $this->app->getContainer()['request'] = Request::createFromEnvironment($env);
        $response = $this->app->run(true);

        $this->assertTrue($this->app->getContainer()->get('auth')->check());
    }

    public function testLoginFail(){
        $this->app->getContainer()->get('csrf')->generateToken();

        $env = \Slim\Http\Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/auth/login',
            'QUERY_STRING' => http_build_query([
                $this->app->getContainer()->get('csrf')->getTokenNameKey() => $this->app->getContainer()->get('csrf')->getTokenName(),
                $this->app->getContainer()->get('csrf')->getTokenValueKey() => $this->app->getContainer()->get('csrf')->getTokenValue(),
                'username' => 'wrong_username',
                'password' => 'wrong_password'
            ])
        ]);

        $this->app->getContainer()['request'] = Request::createFromEnvironment($env);
        $response = $this->app->run(true);

        $this->assertFalse($this->app->getContainer()->get('auth')->check());
    }

    public function testLoginSuccessRedirect(){
        $this->app->getContainer()->get('csrf')->generateToken();

        $env = \Slim\Http\Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/auth/login',
            'QUERY_STRING' => http_build_query([
                $this->app->getContainer()->get('csrf')->getTokenNameKey() => $this->app->getContainer()->get('csrf')->getTokenName(),
                $this->app->getContainer()->get('csrf')->getTokenValueKey() => $this->app->getContainer()->get('csrf')->getTokenValue(),
                'username' => env('ADMIN_MAIL'),
                'password' => env('ADMIN_PASSWORD'),
                'login_redirect' => 'http://www.google.com/coucou'
            ])
        ]);

        $this->app->getContainer()['request'] = Request::createFromEnvironment($env);
        $response = $this->app->run(true);

        $this->assertTrue($this->app->getContainer()->get('auth')->check());
    }
}
?>